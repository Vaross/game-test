===== Traffic Light Game =====

- SETUP:
    * node v18.3.0+
    * Ionic v6.19.1+ (npm install -g @ionic/cli)

- INSTALL:
    * Clone the repo from https://gitlab.com/Vaross/game-test
    * In a terminal open the game-test folder and run (npm install --force)

- DEVELOPMENT:
    * Run (ionic serve)

- TEST:
    * Run (npm test)

- DEPLOY: 
    * Run (ionic build --prod)
    * Run (firebase deploy)
    * The game is live in https://trafficlight-9f5ea.web.app
