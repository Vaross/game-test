// GAME
export const TIME_VARIATION_INTERVAL = [-1500, 1500];
export const GREEN_LIGHT_BASE_TIME = 10000;
export const GREEN_LIGHT_MINIMUM_TIME = 2000;
export const RED_LIGHT_TIME = 3000;
export const TIME_DECREASE_FACTOR = 100;
 
// STORAGE
export const PLAYERS_LIST = 'players';
export const CURRENT_PLAYER = 'currentPlayer';