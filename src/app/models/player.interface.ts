export interface Player {
    name: string;
    highScore: number;
    score: number;
}