import { Injectable } from '@angular/core';
import { TIME_VARIATION_INTERVAL, GREEN_LIGHT_BASE_TIME, GREEN_LIGHT_MINIMUM_TIME, TIME_DECREASE_FACTOR } from 'src/app/constants/constants';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor() { }

  getRandomVariation (): number {
    return TIME_VARIATION_INTERVAL[Math.floor(Math.random()*TIME_VARIATION_INTERVAL.length)];;
  }

  getGreenLightTime(score: number): number {
    const variation = this.getRandomVariation();
    return Math.max(GREEN_LIGHT_BASE_TIME - score * TIME_DECREASE_FACTOR, GREEN_LIGHT_MINIMUM_TIME) + variation;
  }
}
  