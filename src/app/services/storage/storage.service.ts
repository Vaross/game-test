import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { Player } from 'src/app/models/player.interface';
import { PLAYERS_LIST, CURRENT_PLAYER } from 'src/app/constants/constants';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private _storage: Storage | null = null;

  constructor(private storage: Storage) {
    this.init();
  }

  async init() {
    const storage = await this.storage.create();
    this._storage = storage;
    this._storage?.set(PLAYERS_LIST, []);
  }

  public setSessionPlayer(playerName: string){
    this._storage?.set(CURRENT_PLAYER, playerName);
  }

  public async getSessionPlayer(): Promise<Player>{
    let player: Player;
    await this._storage?.get(CURRENT_PLAYER).then(async sessionPlayer => {
      player = await this.getPlayer(sessionPlayer);
    });
    return player;
  }

  public createPlayer(playerName: string) {
    let newPlayer: Player = {name: playerName, highScore: 0, score: 0};
    this.storage.get(PLAYERS_LIST).then(players => {
      let newPlayers = players;
      newPlayers.push(newPlayer);
      this._storage?.set(PLAYERS_LIST, newPlayers);
    }); 
  }

  public async getPlayer(playerName: string): Promise<Player> {
    let player: Player;
    await this._storage?.get(PLAYERS_LIST).then(players => {
      player = players.find(player => player.name === playerName);   
    });
    return player;
  }

  public getPlayers(): Promise<Player[]> {
    return this._storage?.get(PLAYERS_LIST);
  }

  public async updatePlayer(player:Player) {
    await this._storage?.get(PLAYERS_LIST).then(players => {
      let playerIndex = players.findIndex(oldPlayer => oldPlayer.name === player.name);
      const newPlayers = players;
      newPlayers[playerIndex] = player;
      this._storage?.set(PLAYERS_LIST, newPlayers);
    });
  }
  
}
