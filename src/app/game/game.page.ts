import { Component } from '@angular/core';
import { StorageService } from '../services/storage/storage.service';
import { GameService } from '../services/game/game.service'; 
import { Player } from '../models/player.interface';
import { Router } from '@angular/router';
import { GREEN_LIGHT_BASE_TIME, RED_LIGHT_TIME } from 'src/app/constants/constants';


@Component({
  selector: 'app-game',
  templateUrl: 'game.page.html',
  styleUrls: ['game.page.scss']
})
export class GamePage {

  public player: Player = { name: '', highScore: 0, score: 0 }
  public currentFoot: string;
  public walk: boolean = true;
  public trafficLightTime;

  constructor(private storage: StorageService, private gameSvc: GameService,  private router: Router) {
  }

  async ionViewWillEnter() {
    this.player = await this.storage.getSessionPlayer();
    this.initGame();
  }

  exit() {
    this.router.navigate(['/home']);
  }

  initGame() {
    this.setTrafficLightTime(GREEN_LIGHT_BASE_TIME);
    this.currentFoot = '';
  }

  step(foot:string) {
    if (this.walk) {        
      if (foot !== this.currentFoot) {
        this.player.score += 1;
        this.currentFoot = foot;
        this.updateScores();
        const greenLight = this.gameSvc.getGreenLightTime(this.player.score);
        this.setTrafficLightTime(greenLight);
      } else {
        this.player.score -= 1;
      }
    } else {
      this.initGame();
    }
  }

  setTrafficLightTime(timeout: number) {
    window.clearTimeout(this.trafficLightTime);
    this.trafficLightTime = window.setTimeout(() => {
      this.walk = !this.walk;
      window.setTimeout(() => {
        this.walk = true;
        this.setTrafficLightTime(timeout);
      }, RED_LIGHT_TIME);
    }, 
    timeout); 
  }

  updateScores() {
    if (this.player.score >= this.player.highScore) {
      this.player.highScore = this.player.score;
      this.storage.updatePlayer(this.player);
    }
  }

}
