import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Storage } from '@ionic/storage-angular';
import { StorageService } from '../services/storage/storage.service';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { GamePage } from './game.page';
import { HomePage } from '../home/home.page'

describe('GamePage', () => {
  let component: GamePage;
  let fixture: ComponentFixture<GamePage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [GamePage],
      imports: [IonicModule.forRoot(), RouterTestingModule.withRoutes([
        { path: '/home', component: HomePage }
      ])],
      providers: [Storage, StorageService]
    }).compileComponents();

    fixture = TestBed.createComponent(GamePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
