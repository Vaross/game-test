import { Component } from '@angular/core';
import { StorageService } from '../services/storage/storage.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {

  public name: string;

  constructor(private storage: StorageService, private router: Router) {
    this.name = '';
  }

  async ionViewWillEnter() {
    this.name = '';
  }

  async join() {
    const player = await this.storage.getPlayer(this.name);
    if (!player) {
      this.storage.createPlayer(this.name);
    }
    this.storage.setSessionPlayer(this.name);
    this.router.navigate(['/game']);

  }

}
